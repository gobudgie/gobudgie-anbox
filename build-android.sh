#!/bin/bash

set -e

BRANCH="anbox"
#BRANCH="patch-1"
SYNC=8
PLATFORM="https://github.com/anbox/platform_manifests.git"
#PLATFORM="https://github.com/JasMich/platform_manifests.git"


# colors
NC="\e[39m"
RED="\e[31m"
GREEN="\e[32m"

# check if user is root
if [ "$(whoami)" == "root" ]; then
	echo "Sorry, you are root. Please run this script as non-superuser."
	exit 1
fi

# check if repo is installed
if [ ! "$(which repo)" ]; then
	echo -e "repo is not installed. Please install repo.\nExample: sudo apt install repo"
	exit 1
fi

# check if docker is installed
if [ ! "$(which docker)" ]; then
	echo -e "docker is not installed. Please install docker.\nExample: sudo apt install docker"
	exit 1
fi

echo -e "WARNING: THIS WILL ${GREEN}DOWNLOAD${NC} ABOUT >>${RED}150GB${NC}<< OF SOURCE CODE."
while true; do
    read -p "Do you realy whish to continue? y/n: " yn
    case $yn in
        [Yy]* ) true; break;;
        [Nn]* ) exit 1;;
        * ) echo "Please answer yes or no.";;
    esac
done

WD=$(pwd)

if [ ! -d "${WD}/android-work" ]; then
    mkdir ${WD}/android-work
fi

cd ${WD}/android-work
repo init -u ${PLATFORM} -b "${BRANCH}"
repo sync -j${SYNC} -f --force-sync


if [ ! -d "${WD}/docker" ]; then
    mkdir ${WD}/docker
fi
cd ${WD}/docker

cp ~/.gitconfig gitconfig

# Downloading and modifying Dockerfile

curl https://android.googlesource.com/platform/build/+/master/tools/docker/Dockerfile?format=TEXT | base64 --decode > Dockerfile
sed -i '$ d' Dockerfile
echo "USER \$username" >> Dockerfile
sudo docker build --build-arg userid=$(id -u) --build-arg groupid=$(id -g) --build-arg username=$(id -un) -t android-build-trusty .
sudo docker run -it -v ${WD}/android-work/:/src android-build-trusty bash -c "cd /src; source build/envsetup.sh; lunch anbox_x86_64-userdebug; make -j${SYNC}"

cd ${WD}/android-work/vendor/anbox/

scripts/create-package.sh \
        ${WD}/android-work/out/target/product/x86_64/ramdisk.img \
        ${WD}/android-work/out/target/product/x86_64/system.img

mv android.img ${WD}/android.orig.img