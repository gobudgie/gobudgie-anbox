# Gobudgie-Anbox

This repo contains the sugar, Gobudgie Linux adds to Anbox.

## Build Gobudgie-Anbox

### Build the android image

**Attention**: This will download about 150GB of source code. You can easily skip this step.

Please install `docker repo git` first.

```
make build-android
```

### Gobudgie-ize the built rootfs

Please install `squashfs-tools lzip unzip wget tar html-xml-utils` first.

```
make build-rootfs
```

### Build the debian package

We recommend using gobudgie-builder https://gitlab.com/gobudgie/gobudgie-builder/

```
cd gobudgie-anbox/
dpkg-deb --build "src" "path/to/package.deb"
```
